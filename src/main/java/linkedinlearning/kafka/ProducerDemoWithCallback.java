package linkedinlearning.kafka;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class ProducerDemoWithCallback {
    public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger(ProducerDemoWithCallback.class);

        Properties properties = new Properties();
        //https://kafka.apache.org/20/documentation.html#producerconfigs
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);

        for (int i = 0; i < 10; i++) {
            ProducerRecord<String , String> record =
                    new ProducerRecord<>("first-topic", "message from java code" + i);

            producer.send(record, (recordMetadata, e) -> {
                // executes every time a record is successfully sent or an exception thrown
                if (e == null) {
                    logger.info("received - topic: " + recordMetadata.topic() );
                    logger.info("partition : " + recordMetadata.partition());
                    logger.info("offset: " + recordMetadata.offset());
                    logger.info("timestamp: " + recordMetadata.timestamp());
                } else {
                    logger.error("error ", e);
                }

            });
        }

        producer.flush();
        producer.close();
    }
}
