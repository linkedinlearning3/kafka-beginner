package linkedinlearning.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class Main {
    public static void main(String[] args) {

        // course https://www.linkedin.com/learning/learn-apache-kafka-for-beginners/java-consumer?autoplay=true&resume=false


        System.out.println("hello world");

        Properties properties = new Properties();
        //https://kafka.apache.org/20/documentation.html#producerconfigs
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);

        ProducerRecord<String , String> record =
                new ProducerRecord<String, String>("first-topic", "message from java code 1");
        producer.send(record);
        producer.flush();
        producer.close();

    }
}
