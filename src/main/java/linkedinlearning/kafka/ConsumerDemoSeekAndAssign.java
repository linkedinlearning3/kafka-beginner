package linkedinlearning.kafka;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class ConsumerDemoSeekAndAssign {
    public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger(ProducerDemoWithCallback.class);

        Properties properties = new Properties();
        //https://kafka.apache.org/20/documentation.html#producerconfigs
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<String, String>(properties);
        //kafkaConsumer.subscribe(Collections.singleton("first-topic"));
        TopicPartition topicPartition = new TopicPartition("first-topic", 0);
        long offSet = 15L;
        kafkaConsumer.assign(Arrays.asList(topicPartition));
        kafkaConsumer.seek(topicPartition, offSet);
        //kafkaConsumer.subscribe(Arrays.asList("first-topic"));

        //poll
        boolean keepOnReading = true;
        int numberOfMessageToRead = 5;
        int numberOfMessageRead = 0;
        while (keepOnReading) {
            ConsumerRecords<String, String> records = kafkaConsumer.poll(Duration.ofMillis(100));
            for (ConsumerRecord cr : records) {
                numberOfMessageRead += 1;
                logger.info("key: " + cr.key() + "-- value: " + cr.value() );
                //logger.info("partition: " + cr.partition());
                if (numberOfMessageRead >= numberOfMessageToRead) {
                    System.out.println("XX: " + numberOfMessageRead + "-" + numberOfMessageToRead);
                    keepOnReading = false;
                    break;
                }
            }
        }

        logger.info("Exiting....");

    }
}
