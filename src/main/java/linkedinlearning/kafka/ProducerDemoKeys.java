package linkedinlearning.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class ProducerDemoKeys {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Logger logger = LoggerFactory.getLogger(ProducerDemoWithCallback.class);

        Properties properties = new Properties();
        //https://kafka.apache.org/20/documentation.html#producerconfigs
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);

        for (int i = 0; i < 10; i++) {
            String key = "_id " + i;
            ProducerRecord<String , String> record =
                    new ProducerRecord<>("first-topic",
                            key,
                            "message from java code" + i);
            //logger.info("key: _id " + i);
            producer.send(record, (recordMetadata, e) -> {
                // executes every time a record is successfully sent or an exception thrown
                if (e == null) {
//                    logger.info("received - topic: " + recordMetadata.topic() );
//                    logger.info("partition : " + recordMetadata.partition());
//                    logger.info("offset: " + recordMetadata.offset());
//                    logger.info("timestamp: " + recordMetadata.timestamp());

                    logger.info("key: " + key + ", partition: " + recordMetadata.partition());
                } else {
                    logger.error("error ", e);
                }

            }).get(); // testing only
        }

        producer.flush();
        producer.close();
    }
}
